from PIL import Image

def dhash(image, hash_size=8):
    print("dhash function called!", str(image.size))
    image = image.convert("L").resize((hash_size+1, hash_size), Image.ANTIALIAS)
    image.save("altered_image.jpg")

    difference = []
    for row in range(hash_size):
        for col in range(hash_size):
                left_pixel = image.getpixel((row, col))
                right_pixel = image.getpixel((row+1, col))
                difference.append(left_pixel > right_pixel)
    print("difference: ", str(difference))

    decimal_value = 0
    hex_string = []
    for index, value in enumerate(difference):
        if value:
            decimal_value += 2**(index % 8)
        if (index % 8) == 7:
            hex_string.append(hex(decimal_value)[2:].rjust(2, '0'))
            decimal_value = 0

    output = ''.join(hex_string)
    print("output:", output)
    return output

if __name__ == "__main__":
    print("ohh nooo! this is main")
    image = Image.open("./images/test_1.jpg")
    dhash(image)