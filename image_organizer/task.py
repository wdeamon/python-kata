import os, sys
from PIL import Image
import shutil
from imageHashCalculator.calculate import dhash 

_parsed_directory = "./parsedImages"

def handle_image(path, image_name):
    img = Image.open(path)
    print("calculated hash:" + dhash(img))
    directory_name = str(img.size[0]) + "x" + str(img.size[1])
    if not os.path.exists(_parsed_directory):
        os.makedirs(_parsed_directory)

    if not os.path.exists(os.path.join(_parsed_directory, directory_name)):
        os.makedirs(os.path.join(_parsed_directory, directory_name))
    shutil.copy(path, os.path.join(_parsed_directory, directory_name, image_name))

def handle_filesystem():
    count = 0
    for file_ in os.listdir("./images"):
        fullfilename = os.path.join("./images", file_)
        if(fullfilename.endswith(".jpg")):
            count += 1
            handle_image(fullfilename, file_)
            print(fullfilename)


if __name__ == "__main__":
    print("main")
    handle_filesystem()